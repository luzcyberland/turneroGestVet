from django.db import models

# Create your models here.
class Servicio (models.Model):
    id_servicio =models.AutoField(primary_key=True)    
    descripcion = models.CharField(max_length=200)

    def __str__(self):
        return self.descripcion
   