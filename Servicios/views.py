from django.shortcuts import render
from .forms import ServicioForm
from django.shortcuts import render
from custom_decorators.decorators_2 import rol_required
from django.shortcuts import render, redirect
from Servicios.models import *
from django.views import generic
from django.views.generic import UpdateView
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator


# Create your views here.

def add_servicio(request):
    form = ""
    if request.method == 'POST':
        form = ServicioForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/listar_servicios/')
    else: 
        form=ServicioForm
    return render(request,'servicios/agregar_servicio.html',{'form':form})


class ServicioView(generic.ListView):
    model = Servicio
    context_object_name = 'servicio_list' 
    template_name = 'servicios/listar_servicio.html'    
    def get_queryset(self):
        return Servicio.objects.all()


class ServicioUpdate(UpdateView):
    model = Servicio
    fields =[
            'id_servicio',
            'descripcion'
        ]
    template_name = 'servicios/modificar_servicio.html'
    success_url=reverse_lazy('listar_servicios')
    

def eliminar_servicio(request, id_servicio):
    req = Servicio.objects.get(id_servicio=id_servicio)
    req.delete()
    return redirect('/listar_servicios/')