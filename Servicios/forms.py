from django import forms 
from django.forms import fields
from django.forms import widgets
from django.forms.widgets import Widget
from Servicios.models import *


class ServicioForm(forms.ModelForm):
    class Meta:
        model = Servicio
        fields = [
            'id_servicio',
            'descripcion',
        ]
        
        labels = {
            'id_servicio':'ID Servicio',
            'descripcion':'Descripción',
        }
        widget = {'descripcion': forms.TextInput}

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
