from django.contrib.auth.decorators import login_required
from django.urls import path
from django.conf.urls import url, include
from Servicios.views import *

urlpatterns=[
    url('agregar_servicio/',ServicioView,name='agregar_servicio'),
    path('listar_servicios/', login_required(ServicioView.as_view()), name='listar_servicios'),
    path('modificar_servicio/<pk>/',login_required(ServicioUpdate.as_view()), name='modificar_servicio')
]