from django.shortcuts import render
from Turnos.models import Turno
from Turnos.forms import TurnoForm, DeriveTurnoForm
from django.views import generic
from django.views.generic import UpdateView
from django.urls import reverse_lazy
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.shortcuts import render, redirect
from django.views.generic import View
from django.shortcuts import render, get_object_or_404
from django.views.decorators.http import require_POST
from django.http import HttpResponseRedirect
from django.utils.timezone import now
from Servicios.models import Servicio


# Create your views here.

def add_turno(request):
    form = ""
    if request.method == 'POST':
        form = TurnoForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/listar_turnos/')
    else: 
        form=TurnoForm
    return render(request,'turnos/crear_turno.html',{'form':form})



class TurnosListView(generic.ListView):
    model = Turno
    context_object_name = 'turnos_list' 
    template_name = 'turnos/listar_turnos.html'    
    def get_queryset(self):
        return Turno.objects.exclude(status='1').order_by('-id_servicio', 'priority')



def finalizar_turno(request, id_turno):
    turno = Turno.objects.get(id_turno=id_turno)
    print(id_turno)
    turno.finalize()
    return redirect('/listar_turnos/')


def derivar_turno(request, id_turno):

    #Retrieve the original Turno object
    turno = Turno.objects.get(id_turno=id_turno)

    if request.method == 'POST':

        form = DeriveTurnoForm(request.POST)
        if form.is_valid():
            id_servicio = form.cleaned_data['id_servicio']

            # Create a new Turno object based on the original object
            new_turno = Turno(
                id_cliente=turno.id_cliente,
                priority=turno.priority,
                id_servicio=id_servicio,
            )
            new_turno.save()

        # finalize the original Turno Object
            turno.finalize()
            return redirect('/listar_turnos/')

    else:
        servicios = Servicio.objects.all()
        form = DeriveTurnoForm()
    return render(request, 'turnos/derivar_turno.html', {'form': form})


