from django.contrib.auth.decorators import login_required
from django.urls import path
from django.conf.urls import url, include
from Turnos.views import FinalizeTurnoView, TurnosListView, finalizar_turno, derivar_turno
from Turnos.views import FinalizeTurnoView, TurnosListView, finalizar_turno, derivar_turno



urlpatterns=[
    url('agregar_turno/',TurnosListView,name='agregarturno'),
    path('listar_turnos/', login_required(TurnosListView.as_view()), name='listarturnos'),
    path('finalizar_turno/<int:id_turno>/', finalizar_turno, name='finalizarturno'),
    path('derivar_turno/<int:id_turno>/', derivar_turno, name='derivarturno'),
   
]