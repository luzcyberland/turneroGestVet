from django import forms 
from django.forms import fields
from django.forms import widgets
from django.forms.widgets import NumberInput, Widget
from Turnos.models import Turno

class TurnoForm (forms.ModelForm):
    class Meta:
        model = Turno
        fields = (
            'id_turno',
            'id_cliente',
            'id_servicio',
            'priority',
            )
        labels = {
            'id_turno':'ID Turno',
            'id_cliente':'Cliente',
            'id_servicio':'Servicio',
            'priority':'Prioridad',
            }

class DeriveTurnoForm(forms.ModelForm):
    class Meta:
        model = Turno
        fields = (
           # 'id_turno',
            'id_cliente',
            'id_servicio',
            'priority',
            )
        labels = {
            #'id_turno':'ID Turno',
            'id_cliente':'Cliente',
            'id_servicio':'Servicio',
            'priority':'Prioridad',
            }
            

