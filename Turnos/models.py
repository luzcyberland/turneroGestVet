from django.db import models
from django.utils.timezone import now
from Clientes.models import Cliente
from Servicios.models import Servicio

# Create your models here.

class Turno(models.Model):
    id_turno =  models.AutoField(primary_key=True)
    id_cliente = models.ForeignKey(Cliente,models.CASCADE, default=1)
    
    PRIORITIES = (
        ('High', '3ra Edad, Embarazadas, Discapacidad'),
        ('Medium' , 'Clientes corporativos'),
        ('Low','Clientes personales')
    )
    STATUS = (
        ('0','En espera'),
        ('1','Finalizado')
    )

    id_servicio = models.ForeignKey(Servicio,models.CASCADE, default=1)
    priority = models.CharField(choices=PRIORITIES, null=True, max_length=100)
    status = models.CharField(choices=STATUS, null=True, default=0, max_length=20)
    date_start = models.DateTimeField(default= now)
    date_final = models.DateTimeField(null = True)

    
    def derive(self, new_id_servicio):
        '''El metodo derive permite actualizar el servicio asociado a un turno'''
        self.id_servicio = new_id_servicio
        self.date_start = now()

        self.save()
    
    def finalize(self):
        '''Finaliza la atencion del turno'''
        self.date_final = now()
        self.status = '1'
        self.save()


