"""turnero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.contrib.auth.decorators import login_required
from django.contrib.auth import views as auth_views
from django.contrib import admin
from django.urls import path, include
from Roles.views import add_rol, RolesListView, RolUpdate, eliminar_rol
from Usuarios.views import home, logoutUsuario, crear_usuario, UsuariosListView, UsuarioUpdate, eliminar_usuarios
from Clientes.views import add_cliente, ClientesListView, ClienteUpdate, eliminar_cliente
from Servicios.views import *
from Turnos.views import *



urlpatterns = [
    path('admin/', admin.site.urls),
    #Custom URLS
    path('accounts/login/', auth_views.LoginView.as_view(), name='login'),
    path('', home, name='index'),
    path('logout/', logoutUsuario, name='logout'),
    path('crear_rol/', login_required(add_rol), name='crearrol'),
    path('listar_roles/', login_required(RolesListView.as_view()), name='listarroles'),
    path('modificar_rol/<pk>/',login_required(RolUpdate.as_view()), name='modificarrol'),
    path('eliminar_rol/<int:id_rol>/', login_required(eliminar_rol), name='eliminarrol'),
    path('crear_usuario/',login_required(crear_usuario),name='crearusuario'),
    path('listar_usuarios/', login_required(UsuariosListView.as_view()), name='listarusuarios'),
    path('modificar_usuario/<pk>/',login_required(UsuarioUpdate.as_view()), name='modificarusuario'),
    path('eliminar_usuario/<int:id>/', login_required(eliminar_usuarios), name='eliminarusuario'),
    path('crear_cliente/',login_required(add_cliente),name='crearcliente'),
    path('listar_clientes/',login_required(ClientesListView.as_view()),name='listarclientes'),
    path('modificar_cliente/<pk>',login_required(ClienteUpdate.as_view()),name='modificarcliente'),
    path('eliminar_ciente/<int:id_cliente>/',login_required(eliminar_cliente), name='eliminarcliente'),
    path('agregar_servicio/',login_required(add_servicio),name='agregar_servicio'),
    path('listar_servicios/', login_required(ServicioView.as_view()), name='listar_servicios'),
    path('modificar_servicio/<pk>/',login_required(ServicioUpdate.as_view()), name='modificar_servicio'),
    path('eliminar_servicio/<int:id>/', login_required(eliminar_servicio), name='eliminar_servicio'),
    path('listar_turnos/', login_required(TurnosListView.as_view()), name='listarturnos'),
    path('agregar_turno/',login_required(add_turno),name='crearturno'),
    path('listar_turnos/',login_required(TurnosListView.as_view()),name='listarturnos'),
    path('finalizar_turno/<int:id_turno>/', finalizar_turno, name='finalizarturno'),
    path('derivar_turno/<int:id_turno>/', derivar_turno, name='derivarturno'),
]
