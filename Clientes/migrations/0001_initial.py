# Generated by Django 2.2.2 on 2023-03-26 05:39

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Cliente',
            fields=[
                ('id_cliente', models.AutoField(primary_key=True, serialize=False)),
                ('nombre_cliente', models.CharField(max_length=100)),
                ('apellido_cliente', models.CharField(max_length=100)),
                ('cedula_cliente', models.IntegerField()),
                ('sexo', models.PositiveSmallIntegerField(choices=[(1, 'Masculino'), (2, 'Femenino')])),
                ('direccion_cliente', models.CharField(max_length=100)),
                ('telefono_cliente', models.CharField(max_length=100)),
                ('ruc_cliente', models.CharField(max_length=100)),
                ('email_cliente', models.CharField(max_length=100)),
            ],
        ),
    ]
