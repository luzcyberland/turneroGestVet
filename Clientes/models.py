from django.db import models

# Create your models here.

class Cliente(models.Model):
    id_cliente = models.AutoField(primary_key=True)
    nombre_cliente = models.CharField(max_length=100)
    apellido_cliente = models.CharField(max_length=100)
    cedula_cliente = models.IntegerField()
    SEXO_CHOICES = (
        (1, "Masculino"),
        (2, "Femenino"),
    )
    sexo = models.PositiveSmallIntegerField(choices=SEXO_CHOICES)
    direccion_cliente = models.CharField(max_length=100)
    telefono_cliente = models.CharField(max_length=100)
    ruc_cliente = models.CharField(max_length=100, blank=True)
    email_cliente = models.CharField(max_length=100,blank=True)
     
    def __str__(self):
        return self.nombre_cliente + ' ' + self.apellido_cliente 